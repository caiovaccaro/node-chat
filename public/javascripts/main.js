$(function() {
    var socket = io.connect('http://caiovaccaro.com.br'),
        messagesElement = document.getElementById('messages'),
        lastMessageElement = null,
        inputElement = document.getElementById('input');

    function login() {
        var input = $('#username')
          , send = $('#send')
          , event = function() {
            var username = input.val();
            socket.emit('login', username);
        };

        send.click(function() {
          event();  
        });

        input.keydown(function(e) {
            if(e.keyCode === 13) {
                event();
            }
        });

        send.keydown(function(e) {
            if(e.keyCode === 13) {
                event();
            }
        });
    }

    function userTaken(message) {
        var container = $('#login span');

        container.text(message);
    }

    function logged() {
        var login = $('#login')
          , chat = $('#chating');

        login.hide();
        chat.show();
    }

    function addMessage(message, color) {
        var newMessageElement = document.createElement('div'),
            newMessageText = '<span style="color:' + color + ';">' + message + '</span>';

        $(newMessageElement).append(newMessageText);
        messagesElement.insertBefore(newMessageElement, lastMessageElement);
        lastMessageElement = newMessageElement;
    }

    socket.on('login', function() {
        login();
    });

    socket.on('userTaken', function(message) {
        userTaken(message);
    });

    socket.on('logged', function() {
        logged();
    });

    socket.on('serverMessage', function(content, color) {
        addMessage(content, color);
    });

    inputElement.onkeydown = function(keyboardEvent) {
        if(keyboardEvent.keyCode === 13) {
            socket.emit('clientMessage', inputElement.value);
            inputElement.value = '';
            return false;
        } else {
            return true;
        }
    };
});