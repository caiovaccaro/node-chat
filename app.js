// Modules
var express = require('express')
  , routes = require('./routes')
  , http = require('http')
  , path = require('path')
  , stylus = require('stylus')
  , sugar = require('sugar');

// App
var app = express()
  , server = http.createServer(app)
  , io = require('socket.io').listen(server);

// App variables
var usedUsernames = []
  , colors = ['blue', 'fuchsia', 'grey', 'green', 'maroon', 'navy', 'olive', 'purple', 'red', 'teal', 'orange', 'BlueViolet', 'Indigo', 'OrangeRed', 'SeaGreen', 'GoldenRod', 'DarkBlue', 'Crimson']
  , defaultColor = 'black';

// Settings
app.configure(function(){
  app.set('port', process.env.PORT || 4000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(stylus.middleware({ src: __dirname + '/views', dest: __dirname + '/public' }));
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

// Routes
app.get('/', routes.index);

// Socket
io.sockets.on('connection', function(socket) {

  socket.on('login', function(username) {
    if(usedUsernames.indexOf(username) === -1) {
      var color = colors.sample();
      usedUsernames.push(username);
      colors.remove(color);

      socket.set('username', username, function(err) {
        if(err) throw err;

        socket.emit('logged');
        socket.emit('serverMessage', 'Atualmente logado como ' + username);
        socket.broadcast.emit('serverMessage', username + ' entrou.');
      });

      socket.set('color', color, function(err) {
        if(err) throw err;
      });
      
    } else {
      socket.emit('userTaken', 'Desculpe, este nome de usuário já está sendo usado, por favor escolha outro.');
    }
  });

  socket.on('clientMessage', function(content) {
    socket.get('color', function(err, color) {
      if(err) throw err;
      socket.emit('serverMessage', 'Você disse: ' + content, color);
    });

    socket.get('username', function(err, username) {
      if(err) throw err;
      socket.get('color', function(err, color) {
        if(err) throw err;
        if(!username) username = socket.id;
        socket.broadcast.emit('serverMessage', username + ' disse: ' + content, color);
      });
    });
  });

  socket.on('disconnect', function() {
    socket.get('username', function(err, username) {
      if(err) throw err;
      if(!username) username = socket.id;
      socket.broadcast.emit('serverMessage', username + ' saiu.');

      if(usedUsernames.indexOf(username) !== -1) usedUsernames.remove(username);
    });
  });

  socket.emit('login');
});

// Listen
server.listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});